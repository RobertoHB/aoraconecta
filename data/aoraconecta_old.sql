﻿/*CREATE OR REPLACE DATABASE aoraconecta;*/
USE aoraconecta;



CREATE TABLE clientes (
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(100),
  nif varchar(10),
  direccion varchar(200),
  cp int(11),
  poblacion varchar(100),
  provincia varchar(100),
  movil int(11),
  email varchar(200),
  tipo varchar(50), 
  observaciones text,
  PRIMARY KEY(id),
  UNIQUE KEY(nif)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE cuentas (
  id int(11) AUTO_INCREMENT,
  cliente int(11) NOT NULL,
  codpais char(3) NOT NULL,
  dciban int(3) NOT NULL,
  codigo int(5) NOT NULL,
  sucursal int(5) NOT NULL,
  dc int(2) NOT NULL,
  cuenta int(10) NOT NULL,
  PRIMARY KEY(id),
  CONSTRAINT FK_ClientesCuentas FOREIGN KEY (cliente)
    REFERENCES clientes(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE servicios (
  id int(11) AUTO_INCREMENT,
  referencia varchar(50)NOT NULL ,
  nombre varchar(100) NOT NULL,
  descripcion varchar(200) NOT NULL,
  activo boolean,
  PRIMARY KEY(id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE planes (
  id int(11) AUTO_INCREMENT,
  tipo varchar(50)NOT NULL ,
  velocidad int(5) NOT NULL,
  descripcion varchar(200) NOT NULL,
  activo boolean,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE componen (
  id int(11) AUTO_INCREMENT,
  servicio int(11)NOT NULL ,
  plan int(11) NOT NULL,
  descripcion varchar(200) NOT NULL,
  tarifa double(4,2) NOT NULL,
  activo boolean,
  PRIMARY KEY(id),
  UNIQUE(servicio,plan),
  CONSTRAINT FK_Componenservicios FOREIGN KEY (servicio)
  REFERENCES servicios(id),
  CONSTRAINT FK_Componenplanes FOREIGN KEY (plan)
  REFERENCES planes(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE contratan (
  id int(11) AUTO_INCREMENT,
  cliente int(11)NOT NULL ,
  componen int(11) NOT NULL,
  fecha datetime NOT NULL,
  permanencia datetime NOT NULL,
  estado boolean,
  PRIMARY KEY(id),
  CONSTRAINT FK_ContratanClientes FOREIGN KEY (cliente)
  REFERENCES clientes(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE validan (
  id int(11) AUTO_INCREMENT,
  contrato int(11)NOT NULL ,
  documento longblob,
  valido boolean,
  PRIMARY KEY(id),
  CONSTRAINT FK_ValidanContratos FOREIGN KEY (contrato)
  REFERENCES contratan(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

