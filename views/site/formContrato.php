<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\helpers\Url;


?>

<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>-->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
       <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.theme.min.css"></script>
	<!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>-->
	<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>-->
	<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/formulario.css')?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Datos | Contratar</title>
</head>
<body>
	<main>

		<div class="container">
                    <div class="row">
                            <div class="col-sm-12 cabecera">
                                    <img src="<?= Url::to('@web/imagenes/logo.png') ?>"/>
                            </div>
                    </div>

                    <div id="acordeon">
                        
                        <form action="recibiendoForm" method="post"> 
                            <h3>Datos Personales</h3>
				<!--<div class="col-12 datos_personales">-->
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
                                                </div>
						<input type="text" name="nombre" class="form-control" placeholder="Nombre">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
   						</div>
						<input type="text" name="apellidos" class="form-control" placeholder="Apellidos">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-edit"></i></i></span>
   						</div>
						<input type="text" name = "nif" class="form-control" placeholder="NIF o NIE">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
   						</div>
						<input type="text" name = "movil" class="form-control" placeholder="Teléfono de Contacto">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text">@</span>
   						</div>
						<input type="text" name = "mail" class="form-control" placeholder="Email">
					</div>
				<!--</div>-->
			

			
                            <h3>Empresas</h3>
				<!--<div class="col-12 empresas">-->
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
    					</div>
						<input type="text" name = "empresa" class="form-control" placeholder="Nombre de la Empresa">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-edit"></i></i></span>
   						</div>
						<input type="text" name = "cif" class="form-control" placeholder="CIF">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
   						</div>
						<input type="text" name = "dirfact" class="form-control" placeholder="Dirección de Facturación">
					</div>
				<!--</div>-->
			

			
                            <h3>Dirección</h3>
				<!--<div class="col-12 direccion">-->
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
    					</div>
						<input type="text" name = "dir" class="form-control" placeholder="Calle">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" name = "numero" class="form-control" placeholder="Número">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></i></span>
   						</div>
						<input type="text" name = "piso" class="form-control" placeholder="Piso">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" name = "puerta" class="form-control" placeholder="Puerta">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" name = "local" class="form-control" placeholder="Localidad">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" name = "prov" class="form-control" placeholder="Provincia">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" name = "cp" class="form-control" placeholder="Código Postal">
					</div>
				<!--</div>-->
			

			
                            <h3>Datos Bancarios</h3>
				<!--<div class="col-12 banco">-->
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-book-open"></i></span>
    					</div>
						<input type="text" name = "nombrebanco" class="form-control" placeholder="Banco">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-book-open"></i></span>
   						</div>
						<input type="text" name = "numcta" class="form-control" placeholder="Número de Cuenta">
					</div>
					
				<!--</div>-->
		

			
                            <h3>Portabilidad Fijo y/o Móvil</h3>
				<!--<div class="col-12 portabilidades">-->
					
					<!--<h6>Portabilidad Fijo</h6>-->
                                        <div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
    					</div>
                                            <select type="text" name = "tipoporta" class="form-control" placeholder="Tipo de portabilidad">
                                                 <optgroup label="Tipo Portabilidad"> 
                                                    <option value=""></option>
                                                    <option value="0">Fijo</option>
                                                    <option value="1">Movil</option>
                                                 </optgroup>  
                                            </select>   
					</div>
                                        
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
    					</div>
						<input type="text" name = "portanum" class="form-control" placeholder="Número de Teléfono">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
   						</div>
						<input type="text" name = "compania" class="form-control" placeholder="Compañía Actual">
					</div>

<!--					<h6>Portabilidad Móvil</h6>-->
<!--					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
    					</div>
						<input type="text" name = "mail" class="form-control" placeholder="Número de Teléfono">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Compañía Actual">
					</div>-->
                                        <div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
                                                <select type="text" name = "tipotarifa" class="form-control" placeholder="Tipo de tarifa">
                                                    <optgroup> 
                                                         <option value="" disabled select>Contrato/Prepago</option>
                                                        <option value="0">Contrato</option>
                                                        <option value="1">PrePago</option>
                                                    </optgroup> 
                                                   
                                                </select>   
                                        </div>    
<!--					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Postpago o Prepago">
					</div>-->

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
						<input type="text" name="sim" class="form-control" placeholder="ICC Tarjeta SIM">
					</div>
					
				<!--</div>-->
			
                        </div>
                   
			<div class="row enviar">
                            <button type="submit" class="btn btn-info btn-block">Enviar</button>
			</div>

 </form> 
		</div>
	</main> 
</body>
</html>


<script>
     $(document).ready(function() {
            $("#acordeon")accordion();
                
     });
    
   // $(document).ready(function() {
        
        //$('.col-12').css('display','none');

//        $('.toggleable').click(function() {
//             $('.toggleable').toggle();
//        });
        
//    window.addEventListener('load',(e)=>{
//        
//       var elemento = document.querySelectorAll('.toggleable');
//       
//       
//       var elemento_visible = document.querySelectorAll('.col-12');
//       for(let el=0;el < elemento_visible.length;el++){
//           elemento_visible[el].style.display = "none";
//       }
//       
//       for (let c=0;c<elemento.length;c++){
//             elemento[c].addEventListener('click',(ev)=>{
//               var primerel = elemento[c].childNodes[2].className;
//               var clase = (primerel.split(" ")[1]);
//               divdatos = document.querySelector('.'+clase);
////               console.log(divdatos);
//                 if (divdatos.style.display === "none") {
//                    divdatos.style.display = "block";
//                  } else {
//                    divdatos.style.display = "none";
//                }
//               
//           });
//           
//       }
       
       
       
//       console.log(elemento);
//       
//            elemento.addEventListener('click',(ev)=>{
//                divdatos = document.querySelector('.datos_personales');
//               if (divdatos.style.display === "none") {
//                    divdatos.style.display = "block";
//                  } else {
//                    divdatos.style.display = "none";
//                }
//               // datospersonales.classList.toggle();
//           
//       });
        
         
//    });
    
</script>