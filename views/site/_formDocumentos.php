<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/formulario_archivos.css">
	<!-- <link href="http://allfont.es/allfont.css?fonts=montserrat" rel="stylesheet" type="text/css" /> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Documentos | Contratar</title>
</head>
<body>
	<main>

		<div class="container">
			<div class="row">
				<div class="col-12 cabecera">
					<img src="images/logo.png"/>
				</div>
			</div>
			
			<h4>Adjuntar Documentos</h4>
			<div class="row">
				<div class="col-12 form-group">
					<h6>NIF o NIE</h6>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Seleccionar archivo" />
						<span class=""input-group-btn>
							<div class="btn btn-info">...</div>
						</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 form-group">
					<h6>Número de Cuenta</h6>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Seleccionar archivo" />
						<span class=""input-group-btn>
							<div class="btn btn-info">...</div>
						</span>
					</div>
				</div>
			</div>

			<h4>Portabilidades</h4>
			<div class="row">
				<div class="col-12 form-group">
					<h6>Portabilidad Teléfono Fijo</h6>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Seleccionar archivo" />
						<span class=""input-group-btn>
							<div class="btn btn-info">...</div>
						</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 form-group">
					<h6>Portabilidad Teléfono Móvil</h6>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Seleccionar archivo" />
						<span class="input-group-btn">
							<div class="btn btn-info">...</div>
						</span>
					</div>
				</div>
			</div>

			<h4>Empresas</h4>
			<div class="row">
				<div class="col-12 form-group">
					<h6>CIF o Recibo Autónomos</h6>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Seleccionar archivo" />
						<span class=""input-group-btn>
							<div class="btn btn-info">...</div>
						</span>
					</div>
				</div>
			</div>

			<div class="row subir">
				<button type="button" class="btn btn-info btn-block">Subir Archivos</button>
			</div>


			<div class="row">

				<div class="col-md-6 col-sm-12 pie">
					<span class="textoazul">Información/Soporte</span><br/>
					<span><i class="fa fa-phone"></i> &nbsp; 942231563</span><br/>
					<span><i class="fa fa-phone"></i> &nbsp; 942049655</span><br/>
					<i class="fa fa-envelope"></i> &nbsp; <span class="textoazul">info@aoraconecta.com</span><br>
					<img src="images/logo_fondonegro.png"/><br/><br/>
				</div>
				<div class="col-md-6 col-sm-12 pie2">
					<a href="https://www.facebook.com/aoraconecta"><img src="images/facebook.png" alt="facebook"/></a>
					<a href="https://twitter.com/aoraconecta"><img src="images/twitter.png" alt="twitter"/></a>
					<a href="https://www.instagram.com/aoraconecta"><img src="images/instagram.png" alt="instagram"/></a>
				</div>

			</div>

		</div>
	</main>
</body>
</html>