<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\helpers\Url;
use yii\jui\Accordion;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
$tipoPorta = [ 0 => 'Fijo', 1 => 'Movil'];
$tipoTarifa = [ 0 => 'Contrato', 1 => 'Prepago'];

//   $ClasesNombre = [
//        'options' => ['class' => 'input-group-addon'],
//        'inputTemplate' => "{input}<span class='fa fa-user'></span>"];
?>
<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>-->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.theme.min.css"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/formulario.css')?>">
<script src="<?= Url::to('@web/js/sign.js')?>"></script>
<?php


$this->title = 'Formulario de contrato';
$this->params['breadcrumbs'][] = $this->title;
?>
 <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
           Gracias por contactar con nosotros. Se procederá a gestionar su contrato.
        </div>


  <?php endif; ?> 
        
<div class="contrato-form">


    <?php $form = ActiveForm::begin([
        'action' =>['/site/valida']
     ]);       
    ?>
    
<!--  Accordion::widget([
       'items' => [
           [
 
            'header' => 'Section 1',
            'content' => ' <div class="input-group mb-2">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-user"></i></div>
                $form->field($model, 'nombre')->textInput(['maxlength' => true,'placeholder' => "Nombre",'class'=>'form-control'])->label(false)
            </div>
             
        </div>',
        ],
        [
            'header' => 'Section 2',
            'headerOptions' => ['tag' => 'h3'],
            'content' => 'Sed non urna. Phasellus eu ligula. Vestibulum sit amet purus...',
            'options' => ['tag' => 'div'],
        ],
    ],
    'options' => ['tag' => 'div'],
    'itemOptions' => ['tag' => 'div'],
    'headerOptions' => ['tag' => 'h3'],
    'clientOptions' => ['collapsible' => false],
])-->
<div class="contenedor_iframe">
    <iframe class="ventanafirma" style="display: none" max-width="100%" height="auto"></iframe>
</div>
     <div class='opciones'>Datos Personales</div>	
     <div id="datosPersonales" class='opform'>
        <div class="input-group mb-2">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-user"></i></div>
            </div>
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'placeholder' => "Nombre",'class'=>'form-control'])->label(false); ?>
        </div>


        <div class="input-group mb-2">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-user"></i></div>
            </div>
             <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true,'placeholder' => "Apellidos",'class'=>'form-control'])->label(false); ?>
        </div>    

        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-edit"></i></div>
            </div>
        <?= $form->field($model, 'nif')->textInput(['maxlength' => true,'placeholder' => "NIF"])->label(false); ?>
        </div>
          <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
        <?= $form->field($model, 'movil')->textInput(['maxlength' => true,'placeholder' => "Movil"])->label(false); ?>
          </div>
          <div class="input-group mb-3">
            <div class="input-group-addon">
               <div class="input-group-text">@</div>
            </div>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder' => "Email"])->label(false); ?>
          </div>
     </div>
     
    <div class='opciones'>Empresas</div>
     <div id="datosEmpresa" class='opform'>
        <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-user"></i></div>
           </div>
           <?= $form->field($model, 'nomempresa')->textInput(['maxlength' => true,'placeholder' => "Empresa"])->label(false); ?>
        </div>

         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-user"></i></div>
           </div>
           <?= $form->field($model, 'cif')->textInput(['maxlength' => true,'placeholder' => "CIF"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-phone"></i></div>
           </div>
           <?= $form->field($model, 'dirfacturacion')->textInput(['maxlength' => true,'placeholder' => "Direccion Facturacion"])->label(false); ?>
         </div>  
     </div>
     
   <div class='opciones'>Dirección</div>
     <div id="datosDireccion" class='opform'>
        <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'direccion')->textInput(['maxlength' => true,'placeholder' => "Direccion"])->label(false); ?>
        </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'numero')->textInput(['maxlength' => true,'placeholder' => "Número"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'piso')->textInput(['maxlength' => true,'placeholder' => "Piso"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'puerta')->textInput(['maxlength' => true,'placeholder' => "Puerta"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'cp')->textInput(['maxlength' => true,'placeholder' => "CP"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'localidad')->textInput(['maxlength' => true,'placeholder' => "Localidad"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
           </div>
           <?= $form->field($model, 'provincia')->textInput(['maxlength' => true,'placeholder' => "Provincia"])->label(false); ?>
         </div>
     </div>
    
    <div class='opciones'>Datos Bancarios</div>
    <div id="datosBanco" class='opform'>
        <div class="input-group mb-3">
          <div class="input-group-addon">
              <div class="input-group-text"><i class="fa fa-book-open"></i></div>
          </div>
          <?= $form->field($model, 'nombanco')->textInput(['maxlength' => true,'placeholder' => "Banco"])->label(false); ?>
        </div>    
       <div class="input-group mb-3">
          <div class="input-group-addon">
              <div class="input-group-text"><i class="fa fa-book-open"></i></div>
          </div>
          <?= $form->field($model, 'numcuenta')->textInput(['maxlength' => true,'placeholder' => "Cuenta"])->label(false); ?>
       </div>
    </div> 
    
   <div class='opciones'>Portabilidad Fijo y/o Móvil</div>
    <div id="datosPortabilidad" class='opform'>
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
            <?= $form->field($model, 'tipoportabilidad')->dropDownList($tipoPorta, ['maxlength' => true,'prompt' => "Tipo Portabilidad"])->label(false); ?>
        </div>    
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
             <?= $form->field($model, 'portanumero')->textInput(['maxlength' => true,'placeholder' => "Numero a Portar"])->label(false); ?>
        </div>     
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
            <?= $form->field($model, 'portacompania')->textInput(['maxlength' => true,'placeholder' => "Compañia Actual"])->label(false); ?>
        </div>    

        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-mobile"></i></div>
            </div>
             <?= $form->field($model, 'tipotarifa')->dropDownList($tipoTarifa, ['maxlength' => true,'prompt' => "Tipo Tarifa"])->label(false); ?>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-mobile"></i></div>
            </div>
             <?= $form->field($model, 'iccsim')->textInput(['maxlength' => true,'placeholder' => "ICC"])->label(false); ?>
        </div> 
        
        
    </div>
   
   <div class='opciones'>Archivos</div>
    <div id="archivoCif" class='opform'>
        <!--<div class="input-group mb-3">-->
            <!--<div class="input-group-addon">-->
                <div class="input-group-text" style="display:flex;justify-content: flex-start">
                    <i class="fa fa-id-card fa-4x doc" id="cif" style="margin-right:10px;"></i>
                    <i class="fa fa-credit-card fa-4x doc" id="cta" style="margin-right:10px;"></i>
                    <i class="fa fa-file-signature fa-4x doc" id="sign"></i>
                </div>
                
            
        
          <?= $form->field($model, 'imgcif')->fileInput(['maxlength' => true,'placeholder' => "Adjuntar Imagen CIF",'capture'=>"camera",'style'=>"display:none"],
                            ['inputOptions' => ['id' => 'imagencif']])->label(false); ?>
             
          <?= $form->field($model, 'imgcta')->fileInput(['maxlength' => true,'placeholder' => "Adjuntar Imagen Cuenta",'capture'=>"camera",'style'=>"display:none"],
                            ['inputOptions' => ['id' => 'imagencta']])->label(false); ?>
            
          <?= $form->field($model, 'imgfirma')->fileInput(['maxlength' => true,'placeholder' => "Adjuntar Imagen firma",'capture'=>"camera",'style'=>"display:none"],
                            ['inputOptions' => ['id' => 'imagenfirma']])->label(false); ?>
         <!--</div>-->   
       <!--</div>-->     
    </div>
       
              
<!--        <canvas id="myCanvas" style="border:4px solid black;">Firma electronica</canvas>
        <p>
            <input type="button" id="resetSign">Reset</input>
        </p>-->
    
    
    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-dark btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
     <script>
     window.addEventListener("load",(e)=>{
        
     
        
        var elementos = document.querySelectorAll(".help-block");
        for(let c=0;c < elementos.length;c++){
            elementos[c].classList.remove("help-block");
        }
        $('.opform').css('display','none');
        //controlamos el evento click sobre los iconos de foto y obtenemos el tipo de boton pulsado, cif o cta
        //hacemos el click en el inputfile con jq para no mostrar el tedioso boton de seleccionar archivo del inputfile
        $('.fa-4x').click(function(event){
            var tipoboton = 'contratoform-img'+event.target.id;     
            let inputfileclick = "$("+"'"+"#"+tipoboton+"'"+")";
            
             $('#'+tipoboton).click();

        });
        
        $('#sign').click(function(event){
          var fichero = "firma";
          var iframe = document.querySelector(".ventanafirma");
           iframe.frameBorder=0;
           iframe.width="30%";
           iframe.height="250px";
           iframe.style="position: absolute; left: 13%; margin-top: 33px; display:inline;";
           iframe.setAttribute("src", "<?=Url::to(['site/firma'])?>");
           
        });
//        $('h3').click(function(event){
//           var elemento = event.target;
//            if (elemento.is( "h3" )) {
//                 target.children().toggle();
//            }
////            .next('div').attr('id')
//        });
//        
//        
        var elemento_visible = document.querySelectorAll('.opciones');
       
       for(let el=0;el < elemento_visible.length;el++){
          elemento_visible[el].addEventListener('click',(ev)=>{
//            var elemento = elemento_visible[el].nextSibling.nextElementSibling.getAttribute('id');
            var elemento = elemento_visible[el].nextSibling.nextElementSibling;
            
            $('.opform').css('display','none');
            
           if (elemento.style.display === "none") {
               
                elemento.style.display = "block";
              } else {
                  
                elemento.style.display = "none";
              }
//                console.log(elemento_visible[el].parentNode.children.nextElementSibling);
//               var primerel = elemento_visible[el].childNodes[0];
//                       .childNodes[0];
//             
//               console.log(primerel);
//                var nextSectionWithId =   elemento_visible[el].nextAll("section[id]:first");
//                CONSOLE.LOG(nextSectionWithId);
               
           });
       }

//          $('#myCanvas').sign({
//                resetButton: $('#resetSign'),
//                lineWidth: 5,
//                height:300,
//                width:400
//            });
        
     });
         
    </script>