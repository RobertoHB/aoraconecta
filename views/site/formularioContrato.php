<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\helpers\Url;
use yii\jui\Accordion;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
$tipoPorta = [ 0 => 'Fijo', 1 => 'Movil'];
$tipoTarifa = [ 0 => 'Contrato', 1 => 'Prepago'];

//   $ClasesNombre = [
//        'options' => ['class' => 'input-group-addon'],
//        'inputTemplate' => "{input}<span class='fa fa-user'></span>"];
?>
<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>-->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/formulario.css')?>">
<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/firmaelectronica.css')?>">



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>


<?php


$this->title = 'Formulario de contrato';
$this->params['breadcrumbs'][] = $this->title;
?>
 <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
           Gracias por contactar con nosotros. Se procederá a gestionar su contrato.
        </div>


  <?php endif; ?> 
        
<div class="contrato-form">


    <?php $form = ActiveForm::begin([
        'action' =>['/site/valida']
     ]);       
    ?>
    

<!-- <div class="contenedor_iframe">
    <iframe class="ventanafirma" style="display: none" max-width="100%" height="auto"></iframe>
</div>-->
     <div class='opciones'>Datos Personales</div>	
     <div id="datosPersonales" class='opform' hidden>
        <div class="input-group mb-2">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-user"></i></div>
            </div>
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'placeholder' => "Nombre",'class'=>'form-control'])->label(false); ?>
        </div>


        <div class="input-group mb-2">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-user"></i></div>
            </div>
             <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true,'placeholder' => "Apellidos",'class'=>'form-control'])->label(false); ?>
        </div>    

        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-edit"></i></div>
            </div>
        <?= $form->field($model, 'nif')->textInput(['maxlength' => true,'placeholder' => "NIF"])->label(false); ?>
        </div>
          <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
        <?= $form->field($model, 'movil')->textInput(['maxlength' => true,'placeholder' => "Movil"])->label(false); ?>
          </div>
          <div class="input-group mb-3">
            <div class="input-group-addon">
               <div class="input-group-text">@</div>
            </div>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder' => "Email"])->label(false); ?>
          </div>
     </div>
     
    <div class='opciones'>Empresas</div>
     <div id="datosEmpresa" class='opform' hidden>
        <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-user-tie"></i></div>
           </div>
           <?= $form->field($model, 'nomempresa')->textInput(['maxlength' => true,'placeholder' => "Empresa"])->label(false); ?>
        </div>

         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fa fa-edit"></i></div>
           </div>
           <?= $form->field($model, 'cif')->textInput(['maxlength' => true,'placeholder' => "CIF"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-street-view"></i></div>
           </div>
           <?= $form->field($model, 'dirfacturacion')->textInput(['maxlength' => true,'placeholder' => "Direccion Facturacion"])->label(false); ?>
         </div>  
     </div>
     
   <div class='opciones'>Dirección</div>
     <div id="datosDireccion" class='opform' hidden>
        <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-street-view"></i></div>
           </div>
           <?= $form->field($model, 'direccion')->textInput(['maxlength' => true,'placeholder' => "Direccion"])->label(false); ?>
        </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-sort-numeric-up"></i></div>
           </div>
           <?= $form->field($model, 'numero')->textInput(['maxlength' => true,'placeholder' => "Número"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-arrows-alt-v"></i></div>
           </div>
           <?= $form->field($model, 'piso')->textInput(['maxlength' => true,'placeholder' => "Piso"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-door-open"></i></div>
           </div>
           <?= $form->field($model, 'puerta')->textInput(['maxlength' => true,'placeholder' => "Puerta"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-mail-bulk"></i></div>
           </div>
           <?= $form->field($model, 'cp')->textInput(['maxlength' => true,'placeholder' => "CP"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-city"></i></div>
           </div>
           <?= $form->field($model, 'localidad')->textInput(['maxlength' => true,'placeholder' => "Localidad"])->label(false); ?>
         </div>
         <div class="input-group mb-3">
           <div class="input-group-addon">
               <div class="input-group-text"><i class="fas fa-city"></i></div>
           </div>
           <?= $form->field($model, 'provincia')->textInput(['maxlength' => true,'placeholder' => "Provincia"])->label(false); ?>
         </div>
     </div>
    
    <div class='opciones'>Datos Bancarios</div>
    <div id="datosBanco" class='opform' hidden>
        <div class="input-group mb-3">
          <div class="input-group-addon">
              <div class="input-group-text"><i class="fas fa-university"></i></div>
          </div>
          <?= $form->field($model, 'nombanco')->textInput(['maxlength' => true,'placeholder' => "Banco"])->label(false); ?>
        </div>    
       <div class="input-group mb-3">
          <div class="input-group-addon">
              <div class="input-group-text"><i class="fas fa-money-check"></i></div>
          </div>
          <?= $form->field($model, 'numcuenta')->textInput(['maxlength' => true,'placeholder' => "Cuenta"])->label(false); ?>
       </div>
    </div> 
    
   <div class='opciones'>Portabilidad Fijo y/o Móvil</div>
    <div id="datosPortabilidad" class='opform' hidden>
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
            <?= $form->field($model, 'tipoportabilidad')->dropDownList($tipoPorta, ['maxlength' => true,'prompt' => "Tipo Portabilidad"])->label(false); ?>
        </div>    
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fa fa-phone"></i></div>
            </div>
             <?= $form->field($model, 'portanumero')->textInput(['maxlength' => true,'placeholder' => "Numero a Portar"])->label(false); ?>
        </div>     
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fas fa-phone-volume"></i></div>
                                              
            </div>
            <?= $form->field($model, 'portacompania')->textInput(['maxlength' => true,'placeholder' => "Compañia Actual"])->label(false); ?>
        </div>    

        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
            </div>
             <?= $form->field($model, 'tipotarifa')->dropDownList($tipoTarifa, ['maxlength' => true,'prompt' => "Tipo Tarifa"])->label(false); ?>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-addon">
                <div class="input-group-text"><i class="fas fa-sim-card"></i></div>
            </div>
             <?= $form->field($model, 'iccsim')->textInput(['maxlength' => true,'placeholder' => "ICC"])->label(false); ?>
        </div> 
        
        
    </div>
   
   <div class='opciones'>Archivos</div>
    <div id="archivoCif" class='opform' hidden>
        <!--<div class="input-group mb-3">-->
            <!--<div class="input-group-addon">-->
                <div class="input-group-text" style="display:flex;justify-content: flex-start">
                    <figure><i class="fa fa-id-card fa-4x doc" id="cif" style="margin-right:10px;"></i><figcaption>NIF</figcaption></figure>
                    <figure><i class="fa fa-credit-card fa-4x doc" id="cta" style="margin-right:10px;"></i><figcaption>Cuenta</figcaption></figure>
                    <figure><i class="fa fa-signature fa-4x doc" id="sign"></i><figcaption>Firma</figcaption></figure>
                </div>

                
            
        
          <?= $form->field($model, 'imgcif')->fileInput(['maxlength' => true,'placeholder' => "Adjuntar Imagen CIF",'capture'=>"camera",'style'=>"display:none"],
                            ['inputOptions' => ['id' => 'imagencif']])->label(false); ?>
             
          <?= $form->field($model, 'imgcta')->fileInput(['maxlength' => true,'placeholder' => "Adjuntar Imagen Cuenta",'capture'=>"camera",'style'=>"display:none"],
                            ['inputOptions' => ['id' => 'imagencta']])->label(false); ?>
            
          <?=$form->field($model, 'imgfirma_txt')->textInput(['style'=>"display:none"],['inputOptions' => ['id' => 'imagenfirma']])->label(false);?>
         <!--</div>-->   
       <!--</div>-->     
    </div>




   
       
              
<!--        <canvas id="myCanvas" style="border:4px solid black;">Firma electronica</canvas>
        <p>
            <input type="button" id="resetSign">Reset</input>
        </p>-->
    
    
    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-dark btn-block center-block botonenviar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div id="contenedorCanvas" class="container-fluid" width="100%" style="display: none">
      <span id="cerrar_Ventana_firma" style="text-align: right;"><i class="far fa-times-circle fa-2x" id="cerrar_firma"></i><span>
      <h3>Validar Contrato</h3>
      <canvas width="300" height="200" style="border: none;border-radius:5%">
      
      </canvas>

      <div id="contenedor_imag" class="container-fluid">
      </div>  

      <div id="botones">
           <button id="borrar"><i class="fas fa-times fa-2x"></i></button>
           <button id="validar"><i class="fas fa-check fa-2x"></i></button>
           <button id="confirmar"><i class="fas fa-share fa-2x"></i></button>
       </div>

</div>


     <script>
         
    
     window.addEventListener("load",(e)=>{
     
    //Funcionalidad ventana principal    
        var elementos = document.querySelectorAll(".help-block");
        for(let c=0;c < elementos.length;c++){
            elementos[c].classList.remove("help-block");
        }
        
        //ocultamos todos los inputs del formulario
       // $('.opform').css('display','none');
        
        //controlamos el evento click sobre los iconos de foto y obtenemos el tipo de boton pulsado, cif o cta
        //hacemos el click en el inputfile con jq para no mostrar el tedioso boton de seleccionar archivo del inputfile
        $('.fa-4x').click(function(event){
            var tipoboton = 'contratoform-img'+event.target.id;     
            let inputfileclick = "$("+"'"+"#"+tipoboton+"'"+")";
            
             $('#'+tipoboton).click();

        });
        
        $('#sign').click(function(event){
          
          let contenedor_canvas = document.querySelector('#contenedorCanvas');
          contenedor_canvas.style = "top:10%;left:30%;display:inline-block;opacity:1;z-index:100;background-color: white;Max-Width: 100%;overflow: hidden;position: absolute;border:3px solid #1A596C;border-radius:5%";

        });
     
        var elemento_visible = document.querySelectorAll('.opciones');
       
       for(let el=0;el < elemento_visible.length;el++){
          elemento_visible[el].addEventListener('click',(ev)=>{
//          var elemento = elemento_visible[el].nextSibling.nextElementSibling.getAttribute('id');
            var elemento = elemento_visible[el].nextSibling.nextElementSibling;
            
            //cerramos todos los divs del menu del formulario de contrato al seleccionar cualquier opcion
            $('.opform').css('display','none');
            
           if (elemento.style.display === "none") {
               
                elemento.style.display = "block";
              } else {
                  
                elemento.style.display = "none";
              }
     
               
           });
       }

       //----------------------------------

        //VAriables para funcionalidad canvas firma electronica
      let canvas;
      let ctx;
      let borrarLienzo = document.querySelector("#borrar");
      let confirmarLienzo = document.querySelector("#confirmar");
      let validarLienzo = document.querySelector("#validar");
      let contenedor_elemento_img = document.querySelector("#contenedor_imag");
      let cerrar_ventana_firmas = document.querySelector("#cerrar_firma");
       //---------------------------------------------------------------

    canvas = document.querySelector("canvas");
    ctx = canvas.getContext("2d");
    ctx.strokeStyle = "#222222";
    ctx.lineWidth = 4;
    // Set up mouse events for drawing
    var drawing = false;
    var mousePos = { x:0, y:0 };
    var lastPos = mousePos;
    canvas.addEventListener("mousedown", function (e) {
            drawing = true;
      lastPos = getMousePos(canvas, e);
    }, false);
    canvas.addEventListener("mouseup", function (e) {
      drawing = false;
    }, false);
    canvas.addEventListener("mousemove", function (e) {
      mousePos = getMousePos(canvas, e);
    }, false);

    // Get the position of the mouse relative to the canvas
    function getMousePos(canvasDom, mouseEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
      };
    }
    
    // Get a regular interval for drawing to the screen
        window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || 
           window.webkitRequestAnimationFrame ||
           window.mozRequestAnimationFrame ||
           window.oRequestAnimationFrame ||
           window.msRequestAnimaitonFrame ||
           function (callback) {
        window.setTimeout(callback, 1000/60);
           };
        })();
    
    
    // Draw to the canvas
    function renderCanvas() {
      if (drawing) {
        ctx.moveTo(lastPos.x, lastPos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
        lastPos = mousePos;
      }
    }

    // Allow for animation
    (function drawLoop () {
      requestAnimFrame(drawLoop);
      renderCanvas();
    })();

    // Set up touch events for mobile, etc
    canvas.addEventListener("touchstart", function (e) {
            mousePos = getTouchPos(canvas, e);
      var touch = e.touches[0];
      var mouseEvent = new MouseEvent("mousedown", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(mouseEvent);
    }, false);
    canvas.addEventListener("touchend", function (e) {
      var mouseEvent = new MouseEvent("mouseup", {});
      canvas.dispatchEvent(mouseEvent);
    }, false);
    canvas.addEventListener("touchmove", function (e) {
      var touch = e.touches[0];
      var mouseEvent = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(mouseEvent);
      e.preventDefault();
    }, false);

    // Get the position of a touch relative to the canvas
    function getTouchPos(canvasDom, touchEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
      };
    }

    // Prevent scrolling when touching the canvas
    document.body.addEventListener("touchstart", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchend", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchmove", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    

    //Escuchadores de eventos para firma electronica

   //Borramos el canvas y eliminamos el elemento de imagen que guarda el mismo
    borrarLienzo.addEventListener('click', (e)=>{
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       canvas.width = canvas.width;
       ctx.lineWidth = 4;
       let elementBorrar = document.querySelector("#imagen_validada");
       elementBorrar.parentNode.removeChild(elementBorrar);
      
     });
    //guardamos el dibujo del canvas en un elemento de imagen creado previamente al validar el canvas
     confirmarLienzo.addEventListener('click', (e)=>{
      //mostrar_img.src = canvas.toDataURL("image/png"); 
    
        document.querySelector('#contenedorCanvas').style.display = "none";

     });


    //validamos el canvas como firma correcta y creamos el elemento img en ese momento para despues mostrar el resultado en el mismo y poder guardar la imagen en el servidor. 
  validarLienzo.addEventListener('click', (e)=>{

    //crear elemento img
    let newElement = document.createElement("img");
      contenedor_elemento_img.appendChild(newElement);
      
      let newElemento_img = document.querySelector("img");
      newElemento_img.setAttribute("id", "imagen_validada"); 
      newElemento_img.classList.add("img-fluid");
       newElemento_img.style.width = "80%";
      document.querySelector("#confirmar").style.display ="inline";
      
    //asignar imagen canvas en en atributo src de la imagen

      newElemento_img.src = canvas.toDataURL("image/png");
      //console.log(btoa(newElemento_img));
      $('#contratoform-imgfirma_txt').val(newElemento_img.src);
    
      
      
     });

   cerrar_ventana_firmas.addEventListener('click', (e)=>{
          
        document.querySelector('#contenedorCanvas').style.display = "none";

     });




    



        
     });
         
    </script>