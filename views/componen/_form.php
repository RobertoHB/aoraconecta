<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Componen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="componen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'servicio')->textInput() ?>

    <?= $form->field($model, 'plan')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarifa')->textInput() ?>

    <?= $form->field($model, 'activo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
