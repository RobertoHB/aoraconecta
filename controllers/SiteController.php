<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\servicios;
use app\models\planes;
use app\models\componen;
use app\models\ContratoForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
         $dataProvider = new ActiveDataProvider([
             'query' => servicios::find()
                 ->select("nombre,id")->distinct(),
         
         ]);

        
        return $this->render("index",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','id'],
        ]);

   
        
    }
      public function actionPlanescontrato($servicio)
    {



//                 
                $dataProvider = new ActiveDataProvider([    
                'query' => componen::find()
                   ->select("planes.id id_plan,planes.titulo titulo,
                                planes.velocidad velocidad,planes.llamadas llamadas,
                                planes.datos datos,planes.descripcion descripcion,tarifa")
                   ->innerJoinWith('servicios')
                   ->innerJoinWith('planes')  
//                    ->innerJoin('planes','componen.plan = planes.id')
//                    ->innerJoin('servicios','componen.servicio = servicios.id')
                    ->where('componen.servicio ='.$servicio),
                    
           ]);    
               
        
                
       //  ]);
//        $resultado = $dataProvider->getModels();
//        $array_resultado=[];
//        foreach($resultado as $resul){
//            $array_resultado['tipo'] = $resul['tipo']; 
//            $array_resultado['titulo'] = $resul['titulo']; 
//            $array_resultado['vel'] = $resul['vel']; 
//            $array_resultado['tel'] = $resul['tel']; 
//            $array_resultado['dat'] = $resul['dat']; 
//            $array_resultado['descplan'] = $resul['descplan']; 
//            $array_resultado['tar '] = $resul['tar']; 
//        }
       
//        echo("<pre>");
//         var_dump($dataProvider);
//         echo("</pre>");
//         exit;
        
        return $this->render("planes",[
            "resultados"=>$dataProvider,
            "campos"=>['id_plan','titulo','velocidad','llamadas','datos','descripcion','tarifa'],
//            "titulo"=>"Planes del Servicio",
//            "enunciado"=>"Información sobre el plan",
//            "sql"=>"SELECT p.tipo tipo, p.titulo titulo, p.velocidad vel, p.llamadas tel,p.datos dat,p.descripcion descplan,c.descripcion descomponen,c.tarifa tar 
//                                    FROM componen c INNER JOIN servicios s ON c.servicio = s.id 
//                                                    INNER JOIN planes p ON c.plan = p.id
//                                                    WHERE c.servicio = $servicio",

        ]);

   
        
    }


     public function actionInfoplan()
    {
         $miplan = $_POST['plan'];
         if(isset($_POST['plan'])){
             $informacionPlan = new SqlDataProvider([
                'sql' => "select componen.id,componen.tarifa,planes.tipo,planes.titulo,
                                             planes.velocidad,planes.llamadas,planes.datos,
                                             planes.descripcion
                         from componen join planes on componen.plan = planes.id where planes.id = '$miplan'",   
           ]); 
            
           
             //return json_encode($myArray);
//             $info_planes = json_decode($informacionPlan);
//             return $info_planes;
            $resultado = $informacionPlan->getModels();
            $array_plan = json_encode($resultado);
            return $array_plan;
           
           
//             $array_info = ['servicio'=>$resultado[0]['id'],
//                            'tarifa'=>$resultado[0]['tarifa'],
//                            'tipo'=>$resultado[0]['tipo'],
//                            'titulo'=>$resultado[0]['titulo'],
//                            'velocidad'=>$resultado[0]['velocidad'],
//                            'llamadas'=>$resultado[0]['llamadas'],
//                            'datos'=>$resultado[0]['datos'],
//                            'descripcion'=>$resultado[0]['descripcion'],
//                            ];
        
//                                
         }


    }
    
    
    public function actionFormular_contrato(){
      
        $model = new contratoForm();
     
         return $this->render('formularioContrato', [
            'model' => $model,
        ]);
        
       
    }
    
     public function actionValida(){
         //$modeloDatos = $_POST->getModels();
//         var_dump($_POST['ContratoForm']);
//         exit;
      
         $model = new ContratoForm();
            if (Yii::$app->request->isPost) {
              
                if ($model->upload()) {
                    // el archivo fue subido exitosamente
                    return;
                }
            }
         
         
        if ($model->load(Yii::$app->request->post()) && $model->envioFormContrato(Yii::$app->params['adminEmail'])) {
          
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('formularioContrato', [
            'model' => $model,
        ]);    
            
            
     }
     
//      public function actionFirma()
//    {
//      
//         return $this->render('firmaelectronica');
//    
//    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
