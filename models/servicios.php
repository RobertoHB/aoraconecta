<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\Query;

use yii\db\BaseActiveRecord;
/**
 * This is the model class for table "servicios".
 *
 * @property int $id
 * @property string $referencia
 * @property string $nombre
 * @property string $descripcion
 * @property int|null $activo
 *
 * @property Componen[] $componens
 * @property Planes[] $plans
 */
class servicios extends \yii\db\ActiveRecord
{
   public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['referencia', 'nombre', 'descripcion'], 'required'],
            [['activo'], 'integer'],
            [['referencia'], 'string', 'max' => 50],
            [['nombre'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 200],
            [['rutaimg'], 'string', 'max' => 200],
        ];
         return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referencia' => 'Referencia',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'rutaimg' => 'Rutaimg',
            'activo' => 'Activo',
        ];
    }

    /**
     * Gets query for [[Componens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponens()
    {
        return $this->hasMany(Componen::className(), ['servicio' => 'id']);
    }

    /**
     * Gets query for [[Plans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Planes::className(), ['id' => 'plan'])->viaTable('componen', ['servicio' => 'id']);
    }
    
   
    public function beforeSave($insert) {
        /* voy a utilizar estas dos propiedades para saber si el usuario a actualizado los ficheros subidos*/
       $this->imageFile=TRUE;  
          
        /* si no ha subido ningun fichero recupero el valor anterior de los ficheros */
        if(!isset($this->rutaimg)){
        
            $this->rutaimg=  $this->getOldAttribute("rutaimg");
            $this->rutaimg=FALSE;
        }
        return true;
    }
    
     public function afterSave($insert, $changeAttributes) {

        /* comprobar si ha seleccionado algun archivo */
        /* ademas comprobamos que sea un fichero lo que me llegue*/
        /* esto es para que si estoy borrando no me de problemas */
        /* llegaria un string en ese caso */
      
        //if($this->imageFile && is_object($this->rutaimg)){
            
            // $ruta_origen = 'C:/xampp/htdocs/aoraconecta/web/imagenes/';
//             $carpeta = 'C:/xampp/htdocs/aoraconecta/web/imagenes/servicios';
//
//            if(!file_exists($carpeta))
//
//                 $ruta = mkdir($ruta_origen.'servicios',true);
//            else
//                 $ruta = $carpeta;
//            
//            
//            $image = UploadedFile::getInstance($model,'imageFile');
//        
//            $image->saveAs($ruta.'/'.$model->id.$image->getExtension()); 
//            
//          
//            $model->rutaimg = $ruta.'/'.$image->name;
//            
//             $model->save();
             $ruta_origen = 'C:/xampp/htdocs/aoraconecta/web/imagenes/';
             $carpeta = 'C:/xampp/htdocs/aoraconecta/web/imagenes/servicios';
             
            if(!file_exists($carpeta))

                 $ruta = mkdir($ruta_origen.'servicios',true);
            else
                 $ruta = $carpeta;
            
            $image = UploadedFile::getInstance($this,'imageFile');
        
            $image->saveAs($ruta.'/'.$this->id. "." .$image->getExtension()); 
               
            $this->rutaimg = $ruta.'/'.$this->id. "." . $image->getExtension();
            
            //crear directorio
        //\yii\helpers\FileHelper::createDirectory('imagenes/servicios/' . $this->id , 0775, false);
        //  $image = UploadedFile::getInstance($model,'imageFile');
          //$image->saveAs($this->rutaimg.'/'.$model->id.'.'.$image->getExtension()); 
          //$this->rutaimg->saveAs('imagenes/servicios/' . $this->id . ".png", false);
          //$this->rutaimg = $this->rutaimg->name;
       // }
        
        //almacenar los cambios
        $this->updateAttributes(["rutaimg"]);
    }

    public function getRutaimg() {
        return Yii::$app->request->getBaseUrl() . '/imgagenes/servicios/' . $model->id . ".png";
    }

    
//  $image = UploadedFile::getInstance($model,'imageFile');
//            $image->saveAs($ruta.'/'.$model->id.'.'.$image->getExtension()); 
//            
//            $model->rutaimg = $ruta.'/'.$image->name;
 
}
