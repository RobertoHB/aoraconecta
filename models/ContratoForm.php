<?php

namespace app\models;
use \yii\base\Model;
use \yii\helpers\Url;
use \Swift_Attachment;
use yii\web\UploadedFile;


use Yii;

class ContratoForm extends Model
{
    //datos personales
    public $nombre;
    public $apellidos;
    public $nif;
    public $movil;
    public $email;
    //datos de empresa
    public $nomempresa;
    public $cif;
    public $dirfacturacion;
    
    //direccion
    public $direccion;
    public $numero;
    public $piso;
    public $puerta;
    public $localidad;
    public $provincia;
    public $cp;
    //datos bancarios
    public $nombanco;
    public $numcuenta;
    
    //tipo de portabilidad
    public $tipoportabilidad;//fijo o movil
    public $portanumero;
    public $portacompania;
    public $tipotarifa;//prepago o postpago
    public $iccsim;//ICC tarjeta SIM
    
    
    public $verifyCode;//codigo de verificacion .No soy una maquina.
    
    public $imgcif;
    public $imgcta;
    public $imgfirma_txt;

    public function rules()
    {
        return [
            //[['nombre', 'apellidos', 'nif', 'movil','email', 'direccion', 'numero', 'piso','puerta', 'poblacion', 'localidad', 'cp','nombanco','numcuenta'], 'required'],
            [['cp', 'movil'], 'integer'],
            [['nombre', 'apellidos', 'localidad', 'provincia'], 'string', 'max' => 100],
            [['nif'], 'string', 'max' => 10],
            [['direccion', 'email'], 'string', 'max' => 200],
            [['nif'], 'unique'],
            [['email'], 'email'],
            [['imgfirma_txt'], 'string'],
            ['verifyCode', 'captcha'],
        ]; 
         return [
            [['imgcif'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['imgcta'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
           
        ];
        
    }
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
    public function upload(){
        $imagenes_subir = [$this->imgcif,$this->imgcta];
       
       
    
    }
    public function envioFormContrato()
    {
        
        $ruta_origen = 'C:/xampp/htdocs/aoraconecta/web/imagenes/';
        $carpeta = 'C:/xampp/htdocs/aoraconecta/web/imagenes/contratos/';
        $ruta = 'C:/xampp/htdocs/aoraconecta/web/imagenes/contratos/'.$this->nif.'/';
        
        //origen de la foto por defecto para cuando no se reciba ni foto del nif ni de la cta o de la firma
            $origen = "C:/xampp/htdocs/aoraconecta/web/imagenes/contratos/sinImagen/";
        
        

        if(!file_exists($carpeta))
            $ruta_contratos = mkdir($ruta_origen.'contratos/',true);
       
        if(!file_exists($ruta))
            $ruta_nif = mkdir($carpeta.$this->nif,true);
          
            $imagecif = UploadedFile::getInstance($this,'imgcif');
            $imagecta = UploadedFile::getInstance($this,'imgcta');
           // $imagefirma = UploadedFile::getInstance($this,'imgfirma');
            

           if($this->imgfirma_txt != Null){
             //creamos la imagen de firma digital que viene en base64 del modelo que despues subiremos a nuestro servidor
                file_put_contents($ruta.'/'.'firma.png', base64_decode(explode(',',$this->imgfirma_txt)[1]));
           }else{
                copy($origen."firma.png", $ruta."firma.png"); 
           }
            
          
            //si no viene imagen de cif
            if($imagecif != Null){
                $imagecif->saveAs($ruta.'/'.'nif'. "." .$imagecif->getExtension()); 
            }else{
                //imagen vacia por defecto de nif
                 copy($origen."nif.jpg", $ruta."nif.jpg"); 
                          
            }
            if($imagecta != Null){
                $imagecta->saveAs($ruta.'/'.'ctabco'. "." .$imagecta->getExtension()); 
            }else{
                //imagen vacia por defecto de cta
                 copy($origen."ctabco.jpg", $ruta."ctabco.jpg"); 
            }    
           // $imagefirma->saveAs($ruta.'/'.'$imagenBinaria'. "." .$NombreImgFirma->getExtension()); 
            

             $path = "../web/imagenes/contratos/".$this->nif;
             
             $datos_contrato = "<form role='form'>";
             $datos_contrato .= "<h1>Nuevo contrato</h1>";
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Nombre</label>";
             $datos_contrato .= "<p>".$this->nombre."</p></div>";
             
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Apellidos</label>";
             $datos_contrato .= "<p>".$this->apellidos."</p></div>";
             
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Nif</label>";
             $datos_contrato .= "<p>".$this->nif."</p></div>";
            
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Movil</label>";
             $datos_contrato .= "<p>".$this->movil."</p></div>";
            
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Email</label>";
             $datos_contrato .= "<p>".$this->email."</p></div>";
           
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Localidad</label>";
             $datos_contrato .= "<p>".$this->localidad."</p></div>";
             
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Provincia</label>";
             $datos_contrato .= "<p>".$this->provincia."</p></div>";
             
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Tipo Portabilidad</label>";
             $datos_contrato .= "<p>".$this->tipoportabilidad."</p></div>";
         
             $datos_contrato .= "<div class='form-group'>";
             $datos_contrato .= "<label>Numero portar</label>";
             $datos_contrato .= "<p>".$this->portanumero."</p></div></form>";
          
             
            Yii::$app->mailer->compose()
                ->attach($path.'/'.'ctabco.jpg')
                ->attach($path.'/'.'nif.jpg')
                ->attach($path.'/'.'firma.png')    
                ->setTo([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setFrom([Yii::$app->params['senderEmail']])
                ->setReplyTo([Yii::$app->params['senderEmail']])
                ->setSubject('Nuevo contrato')
                ->setTextBody('')
                ->setHtmlBody($datos_contrato)
                ->send();

            return true;
      
        return false;
    }
    


}
