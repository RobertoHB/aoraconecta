<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contratan".
 *
 * @property int $id
 * @property int $cliente
 * @property int $componen
 * @property string $fecha
 * @property string $permanencia
 * @property int|null $estado
 *
 * @property Clientes $cliente0
 * @property Validan[] $validans
 */
class contratan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contratan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente', 'componen', 'fecha', 'permanencia'], 'required'],
            [['cliente', 'componen', 'estado'], 'integer'],
            [['fecha', 'permanencia'], 'safe'],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente' => 'Cliente',
            'componen' => 'Componen',
            'fecha' => 'Fecha',
            'permanencia' => 'Permanencia',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Cliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'cliente']);
    }

    /**
     * Gets query for [[Validans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getValidans()
    {
        return $this->hasMany(Validan::className(), ['contrato' => 'id']);
    }
}
